import random
p1 = input("Player 1: Rock, Paper, Scissors: ").lower()
if(p1 == "rock"):
    p1 = 0
elif(p1 == "paper"):
    p1 = 1
else:
    p1 = 2

p2 = random.randint(0,2)
if(p1 == 2 and p2 == 0):
    print("Player 2 wins")
elif(p1 == 0 and p2 == 2):
    print("Player 1 wins")
elif(p1 > p2):
    print("Player 1 wins")
elif(p2 > p1):
    print("Player 2 wins")
else:
    print("It's a tie")
