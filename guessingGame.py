import random
ans = random.randint(0,100)
x = -1
while x != ans:
    x = int(input("Guess a number between 0 and 100: "))
    if(x < ans):
        print("Too low try again")
    elif(x > ans):
        print("Too high try again")
    else:
        print("Answer is correct")
